export PS1="\[\033[36m\]\u\[\033[m\]@\[\033[32m\]\h:\[\033[33;1m\]\w\[\033[m\]\$ "
export CLICOLOR=1
export LSCOLORS=ExFxBxDxCxegedabagacad
alias ls='ls -GFh'
alias clang++='clang++ -std=c++11 -stdlib=libc++'

# Dont logout on ctrl+d
set -o ignoreeof

# Append to bash history instead of rewriting when closing shell
shopt -s histappend

######### better bash history from https://sanctum.geek.nz/arabesque/better-bash-history/ #########

# Longer bash history
HISTFILESIZE=1000000
HISTSIZE=1000000

# Don't save some commands to history
HISTIGNORE='ls:bg:fg:history'

# Add timestamp
HISTTIMEFORMAT='%F %T '

# Multiline commands should be stored in one line in hisotry
shopt -s cmdhist

# Record history after each command
PROMPT_COMMAND='history -a'

######### end of better bash history #########